const mongoose = require('mongoose');

const companySchema = new mongoose.Schema({
    _id: {
        type: Number,
        required: [true, "The ID is required"],
        minlength: 3
    },
    denumire: String,
    adresa: String,
    cif: String,
    cod_postal: String,
    judet: String,
    numar_reg_com: String,
    telefon: String,
})

module.exports = mongoose.models.Company || mongoose.model('Company', companySchema);