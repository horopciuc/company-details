const SearchedCompany = (props) => {
    return (
        <div>
            <div className="titleRows">
                <div className="titleDesign">Denumire:</div>
                <div className="titleDesign">{props.searchedCompany.denumire}</div>
            </div>
            <div className="titleRows">
                <div className="titleDesign">Adresa:</div>
                <div className="titleDesign">{props.searchedCompany.adresa}</div>
            </div>
            <div className="titleRows">
                <div className="titleDesign">CUI:</div>
                <div className="titleDesign">{props.searchedCompany.cif}</div>
            </div>
            <div className="titleRows">
                <div className="titleDesign">Judet:</div>
                <div className="titleDesign">{props.searchedCompany.judet}</div>
            </div>
            <div className="titleRows">
                <div className="titleDesign">Cod Postal:</div>
                <div className="titleDesign">{props.searchedCompany.cod_postal}</div>
            </div>
            <div className="titleRows">
                <div className="titleDesign">Numar reg. com.:</div>
                <div className="titleDesign">{props.searchedCompany.numar_reg_com}</div>
            </div>
            <div className="titleRows">
                <div className="titleDesign">Telefon:</div>
                <div className="titleDesign">{props.searchedCompany.telefon}</div>
            </div>
        </div>
    )
}

export default SearchedCompany