import React, { useEffect, useState } from 'react'

const Companies = (props) => {
    const [companies, setCompanies] = useState([]);
    const [filterInput, setFilterInput] = useState("");
    const [sortAdresa, setSortAdresa] = useState("");
    const [sortDenumire, setSortDenumire] = useState("");

    useEffect(() => {
        const filteredCompanies = props.companies.filter(company =>
            company.adresa.toLowerCase().includes(filterInput) || company.denumire.toLowerCase().includes(filterInput)
        );
        setCompanies(filteredCompanies);
    }, [props, filterInput]);

    /**
     * UseEffect pentru sortarea in functie de adresa
     */
    useEffect(() => {
        if (sortAdresa.length > 0) {
            if (sortAdresa == 'asc') {
                setCompanies(companies.sort((a, b) => {
                    return (a.adresa < b.adresa) ? -1 : a.adresa > b.adresa ? 1 : 0;
                }));
            } else {
                setCompanies(companies.sort((a, b) => {
                    return (a.adresa < b.adresa) ? 1 : a.adresa > b.adresa ? -1 : 0;
                }));
            }
        }
    }, [sortAdresa]);

    /**
     * UseEffect pentru sortarea in functie de denumire
     */
    useEffect(() => {
        if (sortDenumire.length > 0) {
            if (sortDenumire == 'asc') {
                setCompanies(companies.sort((a, b) => {
                    return (a.denumire < b.denumire) ? -1 : (a.denumire > b.denumire) ? 1 : 0;
                }));
            } else {
                setCompanies(companies.sort((a, b) => {
                    return (a.denumire < b.denumire) ? 1 : (a.denumire > b.denumire) ? -1 : 0
                }));
            }
        }
    }, [sortDenumire]);

    function handleFilterInput(e) {
        setFilterInput(e.target.value.toLowerCase());
    }

    function handleAdresaSort() {
        if (sortAdresa.length == 0) {
            setSortAdresa('asc');
        } else if (sortAdresa == 'asc') {
            setSortAdresa('desc');
        } else {
            setSortAdresa('asc');
        }
    }

    function handleDenumireSort() {
        if (sortDenumire.length == 0) {
            setSortDenumire('asc');
        } else if (sortDenumire == 'asc') {
            setSortDenumire('desc');
        } else {
            setSortDenumire('asc');
        }
    }

    return (
        <>
            <input
                onChange={(e) => handleFilterInput(e)}
                placeholder="Search Name/Address"
                className="searchInput"
            />
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th className="sortableColumn" onClick={() => handleDenumireSort()}>Denumire <i className="sortCol"></i></th>
                        <th>CUI</th>
                        <th className="sortableColumn" onClick={() => handleAdresaSort()}>Adresa <i className="sortCol"></i></th>
                        <th>Judet</th>
                        <th>Cod postal</th>
                        <th>Numar de reg. com.</th>
                        <th>Telefon</th>
                    </tr>
                </thead>
                <tbody>
                    {companies.map(company => (
                        <tr key={company.cif}>
                            <td>{company.denumire}</td>
                            <td>{company.cif}</td>
                            <td>{company.adresa}</td>
                            <td>{company.judet}</td>
                            <td>{company.cod_postal}</td>
                            <td>{company.numar_reg_com}</td>
                            <td>{company.telefon}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    )
}

export default Companies
