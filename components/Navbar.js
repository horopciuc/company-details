import Link from 'next/link';

const Navbar = () => {
    return (
        <nav>
            <Link href="/">
                <a>Home</a>
            </Link>
            <Link href="/companiesdetails">
                <a href="#">Company Details</a>
            </Link>
            <div className="animation start-home"></div>
        </nav>
    )
}

export default Navbar
