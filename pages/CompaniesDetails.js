import Companies from '../components/Companies';
import Navbar from '../components/Navbar';

const CompaniesDetails = (query) => {
    return (
        <div>
            <Navbar/>
            <Companies companies={query.query} />
        </div>
    )
}

CompaniesDetails.getInitialProps = async () => {
    //////////////////////////
    // Fetch all the companies

    const response = await fetch('http://localhost:3000/api/companies', {
        method: 'GET'
    });
    const { data } = await response.json();

    return { query: data }

    // Fetch all the companies
    //////////////////////////
}

export default CompaniesDetails
