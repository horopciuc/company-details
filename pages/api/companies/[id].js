import dbConnect from '../../../utils/dbConnect';
import Company from '../../../models/Company';

dbConnect();

export default async (req, res) => {
    const { method } = req;
    const { idFirma } = req.body;

    switch (method) {
        case 'POST':
            try {
                const company = await Company.findById(idFirma);

                if (!company) {
                    return res.status(200).json({ success: true, message: "Compania poate fi adaugata in baza de date" })
                }

                res.status(401).json({ success: false, message: "Compania este deja in BD" });
            } catch (error) {
                res.status(400).json({ success: false, message: "Actiunea nu a putut fi efectuata" });
            }
            break;
        default:
            res.status(400).json({ success: false, message: "Eroare, caz netratat" });
            break;
    }
}