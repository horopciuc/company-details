import { useState } from 'react';
import Router, { useRouter } from 'next/router';
import Navbar from '../components/Navbar';

const Home = () => {
	const router = useRouter();

	const [cui, setCui] = useState('');

	function searchCui() {
		if (cui.trim().length > 0) {
			router.push(`/${cui}`);
		} else {
			console.log("Specificati CUI");
		}
	}

	return (
		<div>
			<div className="backgroundImage" >
				<Navbar/>
				<div className="centerInput">
					<input onChange={(e) => setCui(e.target.value)} placeholder="CUI"/>
					<button className="btnStyle" onClick={() => searchCui()}>Search</button>
				</div>
			</div>
		</div>
	)
}

export default Home;