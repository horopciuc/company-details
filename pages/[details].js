import axios from 'axios'
import SearchedCompany from '../components/SearchedCompany';
import Navbar from '../components/Navbar';
import fetch from 'isomorphic-unfetch';

const Details = ({query}) => {

    async function addCompany() {
        try {
            const checkPresence = await fetch(`http://localhost:3000/api/companies/${query.cif}`, {
                method: 'POST',
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    idFirma: query.cif
                })

            });
            // Compania nu se afla in BD,
            // deci poate fi adaugata
            if (checkPresence.status == 200) {
                try {
                    const response = await fetch('http://localhost:3000/api/companies', {
                        method: 'POST',
                        headers: {
                            "Accept": "application/json",
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify({
                            "_id": query.cif,
                            "denumire": query.denumire,
                            "adresa": query.adresa,
                            "cif": query.cif,
                            "cod_postal": query.cod_postal,
                            "judet": query.judet,
                            "numar_reg_com": query.numar_reg_com,
                            "telefon": query.telefon
                        })
                    });
                    if (response.status == 201) {
                        console.log("Datele au fost salvate!");
                    } else {
                        console.log("Datele nu au putut fi salvate!");
                    }
                    console.log(response);
                } catch (error) {
                    console.log(error);
                }
            }
        } catch (er) {
            console.log(er);
        }
    }

    return (
        <div className="detailsStyle">
            <Navbar/>
            <SearchedCompany searchedCompany={query} />
            <button className="addBtn btnStyle" onClick={() => addCompany()}>Save</button>
        </div>
    )
}

Details.getInitialProps = async (ctx) => {
    //////////////
    // Apelare API
    const companyCUI = ctx.query.details;
    try {
        const responseSearchedCompany = await axios.get("https://api.openapi.ro/api/companies/" + companyCUI, {
            headers: {
                "x-api-key": process.env.OPEN_API
            }
        });
        const responseData = await responseSearchedCompany.data;
        return {
            query: responseData
        }
    } catch (err) {
        console.log(err)
        return {
            query: []
        }
    }
    // Apelare API
    //////////////
}

export default Details